#!/usr/bin/env bash
###########################
# this bash script is a template for remote system shutdown

###############
## Variables ##
###############
hostname="10.0.0.52"

###############
## main	     ##
###############
echo "Shutting down remote linux system: $hostname"

if ssh -i ~/.ssh/shutdown-$hostname -o PasswordAuthentication=no -o StrictHostKeyChecking=no shutdown@$hostname 'sudo systemctl poweroff'; then
    echo "Shutdown successful"
else
    echo "Shutdown failed"
fi

